---
name: "F(x)tec Pro1"
deviceType: "phone"
image: "https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1603110770/ysxni2y7ulluzwlnw3fl.jpg"
buyLink: "https://store.fxtec.com/product/fxtec-pro1/"
aliases:
  - "QX1000"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "-"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "-"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "-"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "?"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "?"
      - id: "apparmorPatches"
        value: "?"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "-"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "?"
      - id: "sdCard"
        value: "?"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "?"
      - id: "waydroid"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "?"
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "?"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "-"
      - id: "proximity"
        value: "?"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "?"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "?"
deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8998 Snapdragon 835"
  - id: "gpu"
    value: "Qualcomm Adreno 540"
  - id: "rom"
    value: "128GB"
  - id: "ram"
    value: "6GB"
  - id: "android"
    value: "Android 9"
  - id: "battery"
    value: "3200mAh"
  - id: "display"
    value: "1080 x 2160 pixels, 5.99 inch"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "5MP"
---

The [F(x)tec Pro1](https://www.fxtec.com/pro1) is the first Ubuntu Touch phone with a physical Keyboard!
